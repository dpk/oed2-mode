;; -*- lexical-binding: t; -*-
(require 'oed2-specials)
(require 'ivy)

(defface oed2-mode-face
  '((t))
  "Face from which all OED2 mode faces inherit. You can use this to
set a base font which should be used to render all OED2 entries."
  :group 'oed2-faces)

(defface oed2-bold-face
  '((t :weight bold :inherit oed2-mode-face))
  "Face used for text which is bold for an unspecified reason in
OED2 mode."
  :group 'oed2-faces)

(defface oed2-italic-face
  '((t :slant italic :inherit oed2-mode-face))
  "Face used for text which is italic for an unspecified reason in
OED2 mode."
  :group 'oed2-faces)

(defface oed2-headword-face
  '((t :weight bold :inherit oed2-mode-face))
  "Face used for the main headword of entries in OED2 mode.

Default is bold."
  :group 'oed2-faces)

(defface oed2-ipa-face
  '((t :inherit oed2-mode-face))
  "Face used for IPA transcriptions in OED2 mode.

Default is normal, but if your regular Emacs font doesn't have
IPA characters, you may wish to change this to one that does.
(Or set oed2-transcribe-ipa to nil.)"
  :group 'oed2-faces)

(defface oed2-greek-face
  '((t :inherit oed2-cited-form-face))
  "Face used for Greek character transcriptions in OED2 mode.

Default is as oed2-cited-form-face, but if your regular Emacs
font doesn't have Greek characters, you may wish to change this
to one that does. (Or set oed2-transcribe-greek to nil.)"
  :group 'oed2-faces)

(defface oed2-ps-face
  '((t :slant italic :inherit oed2-mode-face))
  "Face used for part-of-speech markers in OED2 mode.

Default is italic."
  :group 'oed2-faces)

(defface oed2-homonym-face
  '((t :inherit oed2-mode-face))
  "Face used to display homonym numbers in OED2 mode.

Ideally, this would be superscript, but Emacs doesn't have a way
to select OpenType superscript features yet, nor do most fonts
you're likely to be using in Eamcs even have superscripts, so the
default is normal text."
  :group 'oed2-faces)

(defface oed2-variant-form-face
  '((t :weight semibold :inherit oed2-mode-face))
  "Face used to display variant forms of headwords in OED2 mode.

Default is semibold."
  :group 'oed2-faces)

(defface oed2-cited-form-face
  '((t :slant italic :inherit oed2-mode-face))
  "Face used to display the cited forms of other words (especially
in etymologies) in OED2 mode.

Default is italic."
  :group 'oed2-faces)

(defface oed2-sense-number-face
  '((t :weight bold :inherit oed2-mode-face))
  "Face used for main sense numbers (1., 2., 3., ...) in OED2 mode.

Default is bold."
  :group 'oed2-faces)

(defface oed2-subsense-number-face
  '((t :slant italic :inherit oed2-mode-face))
  "Face used for subsense numbers ((a), (b), (c), ...) in OED2 mode.

Default is italic."
  :group 'oed2-faces)

(defface oed2-label-face
  '((t :slant italic :inherit oed2-mode-face))
  "Face used for usage labels in OED2 mode.

Default is italic."
  :group 'oed2-faces)

(defface oed2-small-type-face
  '((t :inherit oed2-mode-face))
  "Face from which all kinds of faces which conventionally appear
in small type in the OED inherit.

The default is not to reduce the size of type, but you can change
this and it will affect quotation paragraphs and small-type
notes, etc. automatically."
  :group 'oed2-faces)

(defface oed2-note-face
  '((t :inherit oed2-mode-face))
  "Face used for definition and etymological notes in OED2 mode.

Default is oed2-small-type-face."
  :group 'oed2-faces)

(defface oed2-quotation-paragraph-face
  '((t :inherit oed2-small-type-face))
  "Face used for quotation paragraphs in OED2 mode.

Default is oed2-small-type-face."
  :group 'oed2-faces)

(defface oed2-quotation-date-face
  '((t :weight bold :inherit oed2-quotation-paragraph-face))
  "Face used for the dates of quotations in OED2 mode.

Default is bold."
  :group 'oed2-faces)

(defface oed2-author-face
  '((t :underline t :inherit oed2-quotation-paragraph-face))
  "Face used for the authors of quotations in OED2 mode.

Ideally, this would be small caps, but Emacs doesn't have a way
to select OpenType small caps features yet, nor do most fonts
you're likely to be using in Emacs even have small caps, so the
default is underlined."
  :group 'oed2-faces)

(defface oed2-work-title-face
  '((t :slant italic :inherit oed2-quotation-paragraph-face))
  "Face used for the titles of cited works in OED2 mode.

Default is italic."
  :group 'oed2-faces)

(defface oed2-self-explanatory-combination-face
  '((t :slant italic :weight semibold :inherit oed2-mode-face))
  "Face used for combinations ‘in which each word retains its full
meaning’ in OED2 mode.

Default is semibold-italic."
  :group 'oed2-faces)

(defface oed2-specialized-combination-face
  '((t :weight semibold :inherit oed2-mode-face))
  "Face used for combinations ‘in which the signification is somewhat
specialized’ in OED2 mode.

Default is semibold."
  :group 'oed2-faces)

(defvar oed2-tag-faces
  '(("b" . oed2-bold-face)
    ("i" . oed2-italic-face)
    ("r" . oed2-mode-face)
    ("hw" . oed2-headword-face)
    ("ph" . oed2-ipa-face)
    ("ps" . oed2-ps-face)
    ("hm" . oed2-homonym-face)
    ("vf" . oed2-variant-form-face)
    ("cf" . oed2-cited-form-face)
    ("gk" . oed2-greek-face)
    ;; ("s1" . oed2-sense-number-face)
    ;; ("s2" . oed2-sense-number-face)
    ;; ("s3" . oed2-mode-face)
    ;; ("s4" . oed2-sense-number-face)
    ;; ("s5" . oed2-mode-face)
    ;; ("s6" . oed2-sense-number-face)
    ;; ("s7a" . oed2-subsense-number-face)
    ;; ("s7n" . oed2-subsense-number-face)
    ("la" . oed2-label-face)
    ("gr" . oed2-label-face)
    ("qla" . oed2-label-face)
    ("n" . oed2-note-face)
    ("qp" . oed2-quotation-paragraph-face)
    ("qd" . oed2-quotation-date-face)
    ("a" . oed2-author-face)
    ("w" . oed2-work-title-face)
    ("il" . oed2-self-explanatory-combination-face)
    ("bl" . oed2-specialized-combination-face))
  "Alist mapping tag names in the OED2 to face names used to render
them in OED2 mode.")

(defvar oed2-tag-surround
  '(("e" oed2-add-status oed2-add-status)
    ("ve" oed2-add-status oed2-add-status)
    ("pr" "(" ")")
    ("etym" "[" "]")
    ("qp" oed2-paragraph oed2-paragraph)
    ("s1" oed2-numbered-sense-paragraph)
    ("s2" oed2-numbered-sense-paragraph)
    ("s3" oed2-numbered-sense-paragraph)
    ("s4" oed2-numbered-sense-paragraph)
    ("s5" oed2-numbered-sense-paragraph)
    ("s6" oed2-numbered-sense)
    ("s7a" oed2-numbered-sense)
    ("s7n" oed2-numbered-sense)
    ("sgk" oed2-numbered-sense-paragraph)
    ("par" oed2-paragraph)
    ("n" oed2-paragraph)
    ("ed" "[" "]"))
  "Alist mapping tag names in the OED2 to a pair of procedures
which receive the start and end tag tokens (start-or-end tag-name
attr) and current face stack and return (propertized) text to be
inserted at the opening and closing points of those tags. If the
procedures are just strings, the string is inserted with the
current face stack. If only one procedure is given, only the
start tag is specially treated.")

(defvar oed2-fold-column 72
  "Maximum column used by entries in OED2 mode.")

(defvar oed2-sgml-location "~/OED2.sgml"
  "Where OED2 mode will look for the original OED2 SGML file.")
(defvar oed2-index-location "~/OED2.idx"
  "Where OED2 mode will look for the OED2 index file.")
(defvar oed2-headword-index-location "~/oed2-headword-index.el.gz"
  "Where OED2 mode will look for the OED2 index file.")
(defvar oed2-headword-index nil
  "The headword index used for ivy-read for OED2 headword searches.")

(defvar oed2-keep-sgrep-buffer nil
  "Whether to keep the *sgrep-results* buffer after running a
search (useful for debugging).")

(defvar oed2-entry-start nil
  "The byte position in the SGML file at which the current entry
  begins.")
(defvar oed2-entry-end nil
  "The byte position in the SGML file at which the current entry
  ends.")

(defvar-local oed2-entry-spurious-p nil
  "Non-nil if the current OED2 entry has status spurious.")

(defun oed2-sgrep-search-raw (query)
  "Run an sgrep search in sgrep's native query syntax in the OED2
index file and return the results as a list of lists, each one
being (%s %e %r), where %s and %e are Emacs integers and %r is an
Emacs string. See the `-o format' option in the sgrep man page to
find out what these mean."
  (with-current-buffer (get-buffer-create "*sgrep-results*")
    (erase-buffer)
    ;; call sgrep
    (cd (file-name-directory (expand-file-name oed2-sgml-location)))
    (call-process
     "sgrep" nil '(t nil) nil
     "-o" "%s:%e:%r\n"
     "-x" (expand-file-name oed2-index-location)
     "--"
     query)
    ;; parse the result
    (let ((result-list '()))
      (goto-char (point-min))
      (while (< (point) (point-max))
        (let* ((region-start (oed2--sgrep-read-position))
               (region-end (oed2--sgrep-read-position))
               (region-contents (oed2--sgrep-read-region)))
          (push (list region-start region-end region-contents)
                result-list)
          (skip-chars-forward "\n")))
      (unless oed2-keep-sgrep-buffer
        (kill-buffer "*sgrep-results*"))
      (nreverse result-list))))

(defun oed2--sgrep-read-position ()
  (skip-chars-forward ":")
  (set-mark (point))
  (skip-chars-forward "0-9")
  (string-to-number (buffer-substring (mark) (point))))

(defun oed2--sgrep-read-region ()
  (skip-chars-forward ":")
  (set-mark (point))
  (end-of-line)
  (buffer-substring (mark) (point)))

(defun oed2-sgrep-query (query)
  "Convert a query from a convenient, composable S-expression
notation to the string form used by sgrep."
  (cond ((stringp query) (concat "\"" query "\""))
        ((numberp query) (number-to-string query))
        ((listp query)
         (cond ((memq (car query) ;; infix operators
                     '(in
                       not-in
                       containing
                       not-containing
                       equal
                       not-equal
                       or
                       extracting
                       --
                       _-
                       -_
                       __
                       quote
                       _quote
                       quote_
                       _quote_))
                (concat "("
                        (mapconcat 'oed2-sgrep-query
                                   (cdr query)
                                   (concat
                                    " "
                                    (oed2--sgrep-infix-operator (car query))
                                    " "))
                        ")"))
               ((and (listp (car query))
                     (memq (caar query) '(near near-before)))
                (let ((operator (concat
                                 (subst-char-in-string ?- ?_
                                                       (symbol-name (caar query)))
                                 "("
                                 (mapconcat 'oed2-sgrep-query (cdar query) ", ")
                                 ")")))
                  (concat "("
                          (mapconcat 'oed2-sgrep-query
                                   (cdr query)
                                   (concat
                                    " "
                                    operator
                                    " "))
                          ")")))
               ((eq (car query) 'elt)
                (oed2-sgrep-query `(-- (stag ,(upcase (cadr query)))
                                       (etag ,(upcase (cadr query))))))
               ((numberp (car query))
                (concat "["
                        (mapconcat (lambda (start-end)
                                     (format "(%d,%d)"
                                             (car start-end)
                                             (cadr start-end)))
                                   (seq-partition query 2) " ")
                        "]"))
               (t (concat (symbol-name (car query))
                          "("
                          (mapconcat 'oed2-sgrep-query (cdr query) ", ")
                          ")"))))))

(defun oed2--sgrep-infix-operator (sym)
  (let ((infix-name (symbol-name sym)))
    (if (string-prefix-p "not-" infix-name)
        (concat "not " (substring infix-name 4))
      (subst-char-in-string ?- ?. infix-name))))

(defun oed2-sgrep-search (query)
  "Run an sgrep search in S-expression-notation in the OED2 index
file and return the results as a list of lists, each one
being (%s %e %r), where %s and %e are Emacs integers and %r is an
Emacs string. See the `-o format' option in the sgrep man page to
find out what these mean."
  (oed2-sgrep-search-raw (oed2-sgrep-query query)))

(defun oed2-phrase-query (query-word)
  (let ((words (split-string query-word "[^a-zA-Z0-9&;]+")))
    `((near-before 2)
      ,@(mapcar (lambda (word)
                  `(word ,(downcase
                           (replace-regexp-in-string
                            "[^a-zA-Z&;]"
                            (lambda (c)
                              (let ((entity (rassoc c oed2-specials)))
                                (if entity
                                    (concat "&" (car entity) ";")
                                  (error "Unknown character %s" c))))
                            word))))
                words))))

(defun oed2-find-headgroups (word)
  "Find the regions for the headgroups for a search for the given
word."
  ;; TODO: Add an optional part-of-speech and homonym number filter
  ;; (in particular, for the xref matcher).

  ;; TODO: (Optionally) filter out HGs where the HW isn't an exact-ish
  ;; match. A search for 'set' should not return 'dead set' or
  ;; 'avocet, -set'.
  (oed2-sgrep-search `(containing (elt "HG")
                                  (containing (elt "HW")
                                              ,(oed2-phrase-query word)))))

(defun oed2-entries-for-author-query (author)
  `(containing (or (elt "E") (elt "VE"))
               (containing (elt "A")
                           ,(oed2-phrase-query author))))
(defun oed2-quotations-for-author-query (author)
  `(containing (elt "Q")
               (containing (elt "A")
                           ,(oed2-phrase-query author))))

(defun oed2-entry-headgroup (entry)
  "Given OED2 entry source, return only the headgroup."
  (let ((short-entry (substring entry 0 (min 1000 (length entry)))))
    (string-match "<hg>.*</hg>" short-entry)
    (match-string 0 short-entry)))

(defun oed2-find-entry-containing (region-start region-end &optional region-contents)
  "Find the entry (E or VE) containing the given region.

(region-contents is ignored, it's just there to make this easier
to use with apply with a result from an oed2-sgrep-search.)"
  (car
   (oed2-sgrep-search `(containing (or (elt "E")
                                      (elt "VE"))
                                   (,region-start ,region-end)))))

(defun oed2-find-entry-before (entry-start)
  "Find the entry (E or VE) beginning immediately before the
given start point of another entry."
  (car
   (oed2-sgrep-search `((near-before 0) (or (elt "E")
                                          (elt "VE"))
                                    (,entry-start ,entry-start)))))

(defun oed2-find-entry-after (entry-end)
  "Find the entry (E or VE) beginning immediately after the given
end point of another entry."
  (car
   (oed2-sgrep-search `(containing (or (elt "E")
                                       (elt "VE"))
                                   (,(1+ entry-end) ,(1+ entry-end))))))

;;; Interactive Interface
(defun oed2 (key)
  "Search the Oxford English Dictionary, Second Edition (1989) by
prompting for a search mode. Currently supported search modes are:

- h to find an entry by headword
- a to list quotations by a particular author"
  (interactive "kOED2: entry by head(w)ord, or quotations by (a)uthor? ")
  (cond ((equal key (kbd "w"))
         (oed2-headword-search))
        ((equal key (kbd "a"))
         (oed2-author-search (read-from-minibuffer "Who?: ")))
        (t (message "Invalid selection"))))

(defun oed2-headword-index ()
  (when (null oed2-headword-index)
    (load oed2-headword-index-location))
  oed2-headword-index)
(defun oed2-headword-search (&optional initial-input)
  (interactive)
  (let ((entry (ivy-read "OED2 Entry: " (oed2-headword-index)
                         :initial-input (if initial-input
                                            (downcase initial-input)))))
    (if-let ((pos (get-text-property 0 'oed2-location entry)))
        (apply #'oed2-show-entry (oed2-find-entry-after pos)))))
(defun oed2-headword-search/word-at-point ()
  (interactive)
  (oed2-headword-search (thing-at-point 'word)))
(define-key oed2-mode-map (kbd "w") #'oed2-headword-search/word-at-point)
(define-key oed2-mode-map (kbd "W") #'oed2-headword-search)

(define-button-type 'oed2-entry-button
  'action #'oed2-entry-button-action
  'follow-link t)

(defun oed2-entry-button-action (button)
  (let ((entry (apply #'oed2-find-entry-containing
                      (button-get button 'oed2-entry-headgroup))))
    (apply #'oed2-show-entry entry)))

(defun oed2-author-search (author)
  (let* ((quotations
          (oed2-sgrep-search (oed2-quotations-for-author-query author)))
         (entries
          (nreverse (oed2-sgrep-search (oed2-entries-for-author-query author))))
         (quots-by-entry
           (seq-group-by
            (lambda (quot)
              (if (< (caar entries) (car quot) (cadar entries)) (car entries)
                (setq entries (cdr entries))
                (car entries)))
            quotations)))
    (with-output-to-temp-buffer "*oed2-search*"
      (with-current-buffer "*oed2-search*"
        (erase-buffer)
        (insert (format "%d quots in %d entries\n"
                        (length quotations)
                        (length quots-by-entry)))
        (dolist (entry-w/quots quots-by-entry)
          (insert "\n")
          (let ((hg (oed2-entry-headgroup (nth 2 (car entry-w/quots))))
                (beg (point)))
            (oed2-parse-and-render hg "*oed2-search*")
            (let ((end (- (point) 1)))
              (make-button beg end
                           'oed2-entry-headgroup (cadr entry-w/quots)
                           :type 'oed2-entry-button)))
          (dolist (quot (cdr entry-w/quots))
            (oed2-parse-and-render (nth 2 quot) "*oed2-search*")))))))

(defun oed2-previous-entry ()
  "Show the previous entry to the one currently being shown."
  (interactive nil oed2-mode)
  (apply #'oed2-show-entry (oed2-find-entry-before oed2-entry-start)))

(defun oed2-next-entry ()
  "Show the next entry to the one currently being shown."
  (interactive nil oed2-mode)
  (apply #'oed2-show-entry (oed2-find-entry-after oed2-entry-end)))

(defvar oed2-mode-map
  (let ((map (make-keymap)))
    (suppress-keymap map t)
    (define-key map (kbd "n") #'oed2-next-entry)
    (define-key map (kbd "p") #'oed2-previous-entry)
    map)
  "Keymap for commands available when viewing OED2 entries")

(define-derived-mode oed2-mode special-mode "OED2"
  "Mode for viewing OED2 entries"
  :interactive nil)

(defun oed2-show-entry (start end contents)
  "Show a complete entry in the *oed2-result* buffer."
  (setq oed2-entry-start start)
  (setq oed2-entry-end end)
  (oed2-parse-and-render contents))

(defun oed2-parse-and-render (source &optional buffer)
  "Parse and render the source (which need not be a complete entry)
into the given buffer, or a new *oed2-result* temporary buffer if
none is given."
  (if buffer
      (oed2--parse-and-render source buffer)
    (with-output-to-temp-buffer "*oed2-result*"
      (oed2--parse-and-render source (get-buffer-create "*oed2-result*") t))))

(defvar oed2-debug-on nil)
(defun oed2-debug (string &rest info)
  (if oed2-debug-on
      (apply 'message string info)))

(defun oed2--parse-and-render (source-str dest &optional modeify)
  (let ((src (get-buffer-create "*oed2-source*")))
    (with-current-buffer src
      (erase-buffer)
      (insert source-str)
      (goto-char (point-min))
      (let ((elt-stack '())
            (face-stack '()))
        (while (< (point) (point-max))
          (set-mark (point))
          (skip-chars-forward "^<& -")
          (if-let ((chunk (buffer-substring (mark) (point))))
            (oed2--add-chunk chunk dest face-stack))
          (cond ((eq (char-after) ?<)
                 (let* ((tag (oed2--parse-tag))
                        (startp (eq (car tag) 'start))
                        (name (cadr tag))
                        (attrs (caddr tag)))
                   (oed2-debug "tag %s" tag)
                   (cond (startp
                          (push name elt-stack)
                          (if-let ((surround-fn (alist-get name
                                                           oed2-tag-surround
                                                           nil nil
                                                           'cl-equalp)))
                              (if (stringp (car surround-fn))
                                  (oed2--add-chunk (car surround-fn)
                                                   dest face-stack)
                                (funcall (car surround-fn)
                                         tag face-stack dest)))
                          (if-let ((face (alist-get name oed2-tag-faces
                                                    nil nil 'cl-equalp)))
                              (progn
                                (push face face-stack)
                                (oed2-debug "push face %s" face))))
                         (t (cl-assert (string-equal (pop elt-stack) name)
                                       t "unexpected close tag" name)
                            (if-let ((face (alist-get name oed2-tag-faces
                                                      nil nil 'cl-equalp)))
                                (progn
                                  (oed2-debug "pop face %s" face)
                                  (cl-assert (eq (pop face-stack) face) t)))
                            (if-let ((surround-fn
                                      (alist-get name oed2-tag-surround
                                                 nil nil 'cl-equalp)))
                                (cond ((null (cdr surround-fn)) (ignore))
                                      ((stringp (cadr surround-fn))
                                       (oed2--add-chunk (cadr surround-fn)
                                                        dest face-stack))
                                      (t (funcall (cadr surround-fn)
                                                  tag face-stack dest))))))))
                ((eq (char-after) ?&)
                 (if (member "ph" elt-stack)
                     (progn
                       (oed2--add-chunk "&" dest face-stack)
                       (forward-char))
                   (let ((special (oed2--parse-special)))
                     (cond ((string-equal special "[es]") ;; em space
                            (oed2--write-space 2 dest face-stack))
                           ((string-equal special "[ts]") ;; thin space
                            (oed2--write-space 1 dest face-stack))
                           (t (oed2--add-chunk special dest face-stack))))))
                ((eq (char-after) ?\s)
                 (oed2--write-space (skip-chars-forward " ")
                                    dest face-stack))
                ((eq (char-after) ?-)
                 (forward-char)
                 (oed2--add-chunk "-" dest face-stack)
                 (oed2--write-space 0 dest face-stack))
                (t (error "stopped in a place we didn't expect: %d" (point)))))
        (oed2-write-newline dest)))
    (kill-buffer src)
    (if modeify
        (with-current-buffer dest
          (oed2-mode)))))

(defun oed2-paragraph (tag face-stack dest)
  "OED2 surround function to add a paragraph break."
  (oed2-write-newline dest t))
(defun oed2-numbered-sense (tag face-stack dest)
  "OED2 surround function to add a sense number and status mark
if appropriate."
  (oed2-add-status tag face-stack dest)
  (oed2-add-sense-number tag face-stack dest))
(defun oed2-numbered-sense-paragraph (tag face-stack dest)
  "OED2 surround function to add a paragraph break with sense
number and status mark if appropriate."
  (oed2-paragraph tag face-stack dest)
  (oed2-numbered-sense tag face-stack dest))

(defun oed2-add-status (tag face-stack dest)
  (if (eq (car tag) 'start)
      (if-let ((status (alist-get "st" (caddr tag) nil nil 'cl-equalp)))
          (oed2--add-chunk
           (cond ((string-equal status "obs") "†")
                 ((string-equal status "ali") "‖")
                 ((string-equal status "err") "¶")
                 ((string-equal status "spu")
                  (setq oed2-entry-spurious-p t)
                  "[")
                 (t (message "Unknown status: %s" status) ""))
           dest
           face-stack))
    (when (and oed2-entry-spurious-p (or (string-equal (cadr tag) "e")
                                         (string-equal (cadr tag) "ve")))
      (oed2--write-chunk-buffer dest)
      (with-current-buffer dest
        (while (memq (preceding-char) '(?\s ?\n)) (delete-backward-char 1)))
      (oed2--add-chunk "]" dest face-stack))))
(defun oed2-add-sense-number (tag face-stack dest)
  (if-let ((number (alist-get "num" (caddr tag) nil nil 'cl-equalp)))
      (cond ((string-equal (cadr tag) "sgk")
             (oed2--add-chunk "(" dest face-stack)
             (oed2--add-chunk
              (oed2-transcribe number oed2-greek-transcription)
              dest
              (cons 'oed2-subsense-number-face face-stack))
             (oed2--add-chunk ") " dest face-stack))
            ((or (string-equal (cadr tag) "s3")
                 (string-equal (cadr tag) "s5"))
             (oed2--add-chunk (concat (make-string (string-to-number number)
                                                   ?*)
                                      " ")
                              dest
                              face-stack))
            ((or (string-equal (cadr tag) "s7a")
                 (string-equal (cadr tag) "s7n"))
             (oed2--add-chunk "(" dest face-stack)
             (oed2--add-chunk
              number
              dest
              (cons 'oed2-subsense-number-face face-stack))
             (oed2--add-chunk ") " dest face-stack))
            (t
             (oed2--add-chunk (concat number ". ")
                              dest
                              (cons 'oed2-sense-number-face face-stack))))))

(setq oed2--chunk-buffer '())

(defun oed2--add-chunk (word dest face-stack)
  (oed2-debug "adding chunk %s %s" word face-stack)
  (push (list word face-stack) oed2--chunk-buffer))

(defun oed2--parse-tag ()
  (cl-assert (= (skip-chars-forward "<") 1))
  (let* ((startp (zerop (skip-chars-forward "/")))
         (tag-name
          (progn
            (set-mark (point))
            (skip-chars-forward "^> ")
            (buffer-substring (mark) (point))))
         (attrs '()))
    (set-mark (point))
    (while (not (zerop (skip-chars-forward " ")))
      (push (oed2--parse-attribute) attrs))
    (skip-chars-forward ">")
    (list (if startp 'start 'end)
          tag-name
          attrs)))

(defun oed2--parse-attribute ()
  (let* ((attr-name
          (progn
            (set-mark (point))
            (skip-chars-forward "^=")
            (buffer-substring (mark) (point))))
         (attr-value
          (progn
            (skip-chars-forward "=")
            (set-mark (point))
            (skip-chars-forward "^> ")
            (buffer-substring (mark) (point)))))
    (cons attr-name attr-value)))

(defun oed2--parse-special ()
  (cl-assert (= (skip-chars-forward "&") 1))
  (let ((name
         (progn
           (set-mark (point))
           (skip-chars-forward "^;")
           (buffer-substring (mark) (point)))))
    (forward-char)
    (if-let ((mapped (alist-get name oed2-specials nil nil 'string-equal)))
        mapped
      (concat "[" name "]"))))

(defun oed2-transcribe (str table)
  "Transcribe the given string according to the given
transliteration table."
  (mapconcat
   (lambda (char)
     (alist-get (string char) table (string char) nil 'string-equal))
   str ""))

(defun oed2--chunk-length ()
  (apply '+ (mapcar (lambda (x) (length (car x))) oed2--chunk-buffer)))

(defun oed2--write-space (n dest face-stack)
  (with-current-buffer dest
    (if (and oed2-fold-column
             (> (+ (- (point) (line-beginning-position))
                   (oed2--chunk-length)
                   n)
                oed2-fold-column))
           (oed2--write-newline dest))
    (oed2--write-chunk-buffer dest)
    (insert (propertize (make-string n ?\s) 'face face-stack))))

(defun oed2-write-newline (dest &optional indent)
  "Write a newline with an optional paragraph indent."
  (oed2--write-space 0 dest '())
  (oed2--write-newline dest indent))

(defun oed2--write-newline (dest &optional indent)
  (with-current-buffer dest
    (delete-horizontal-space)
    (if (not (= (point) (line-beginning-position)))
        (insert "\n"))
    (if indent
        (insert "  "))))

(defun oed2--write-chunk-buffer (dest)
  (with-current-buffer dest
    (dolist (chunk (reverse oed2--chunk-buffer))
      (oed2--write-chunk chunk dest))
    (setq oed2--chunk-buffer '())))

(defun oed2--write-chunk (chunk dest)
  (with-current-buffer dest
    (let ((text (car chunk))
          (face-stack (cadr chunk)))
      (oed2-debug "write chunk \"%s\" with face-stack %s" text face-stack)
      (insert (propertize
               (cond ((memq 'oed2-ipa-face face-stack)
                      (oed2-transcribe text oed2-ipa-transcription))
                     ((memq 'oed2-greek-face face-stack)
                      (oed2-transcribe text oed2-greek-transcription))
                     (t text))
               'face face-stack)))))

(provide 'oed2)
