;; -*- lexical-binding: t; -*-
(require 'oed2-specials)
(require 'oed2)

(defun oed2-generate-headword-index ()
  (setq oed2-headword-index
        (let ((hgbuf (get-buffer-create "*oed2-headword*"))
              (oed2-fold-column nil)
              (oed2-specials
               (cons '("sm" . "")
                     (cons '("smm" . "") oed2-specials))))
          (mapcar
           (lambda (res)
             (with-current-buffer hgbuf
               (let ((buffer-undo-list t))
                 (erase-buffer)
                 (oed2-parse-and-render (nth 2 res) hgbuf)
                 (let ((hw-str (string-chop-newline
                                (buffer-substring (point-min) (point-max)))))
                   (add-text-properties
                    0 1
                    `(oed2-location ,(nth 0 res))
                    hw-str)
                   hw-str))))
           (oed2-sgrep-search '(elt "HG"))))))

(defun oed2-write-headword-index ()
  (oed2-generate-headword-index)
  (with-temp-file "oed2-headword-index.el"
    (insert "(setq oed2-headword-index \n'")
    (print oed2-headword-index (current-buffer))
    (insert ")\n")))
