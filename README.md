# `oed2.el`

`oed2.el` allows you to search and browse the <cite>Oxford English Dictionary</cite> Second Edition (1989) within GNU Emacs.

<img src="oed2-el.png" width=707 height=844 alt="">

## Dependencies

For installation:

- a Plan 9 system or a working installation of [Plan 9 from User Space][plan9port]. Last time I tried this on Mac OS, the Plan 9 program involved didn’t work properly, but it worked on plan9port on FreeBSD.
- the 1992 release of [the <cite>Oxford English Dictionary</cite>, Second Edition on CD-ROM][oed2cd] for PC or Macintosh; more specifically, the file `OED2.DAT`, which in my case (and the case of the author of the Plan 9 program you need to extract the data) had the SHA-1 hash `626fab18cc9a25feafcf4080901c834e3ca05af7`. Other versions may not work.
- [`decompress.c` and `dump.c`,][tristan-oed] utitilies to turn the `OED2.DAT` file back into the original SGML data by ‘tristan’, a contributor to the Plan 9 tree.

[plan9port]: https://9fans.github.io/plan9port/
[oed2cd]: https://www.worldcat.org/title/oxford-english-dictionary/oclc/63526969
[tristan-oed]: https://9p.io/sources/contrib/tristan/oed/

For running:

- probably GNU Emacs 28 or later — I just (April 2022) adapted the code to create non-interactive modes and specify that commands only apply in the OED2 buffer modes, which are Emacs 28 specific features
- [sgrep][]; I have version 1.94a from [Homebrew][hbsg] on Mac OS.

[sgrep]: https://www.cs.helsinki.fi/u/jjaakkol/sgrep.html
[hbsg]: https://github.com/Homebrew/homebrew-core/blob/HEAD/Formula/sgrep.rb

## Installing

### Building `dump` and `decompress`

On plan9port, use `9c` the compiler and `9l` the linker to obtain usable binaries:

```shell
9 9c dump.c
9 9l -o dump dump.o
9 9c decompress.c
9 9l -o decompress decompress.o
```

On an actual Plan 9 or 9front system, it’ll look fairly similar, but you’ll need to use the right compiler command for your platform and no `9` wrapper command.

### Extracting the OED2 SGML file

As it says in [tristan’s `README` file](https://9p.io/sources/contrib/tristan/oed/README):

```shell
9 ./dump 0x0 0x4 < OED2.DAT | 9 hd
9 ./dump 0x40 0x44 < OED2.DAT | 9 hd
```

Note down the two hex numbers and add `0x8000` to the first one, then use `dump` on that range together with `decompress` to get the SGML file. If your file has the same SHA-1 hash as mine and tristan’s (which it hopefully does) you can use these numbers:

```shell
9 ./dump 0x16709000 0x25bb1000 < OED2.DAT | 9 ./decompress > OED2.sgml
```

Success!

### Building the main index file with `sgrep`

The original `OED2.sgml` uses a variety of SGML where `.`, rather than `;`, is used to terminate the names of character entities. `sgrep`’s SGML mode doesn’t understand this, but it’s easily fixed:

```shell
mv OED2.sgml OED2.orig.sgml
perl -pe 's/(&[^.\s]+)\./\1;/g' < OED2.orig.sgml > OED2.sgml
```

Then we need to tell `sgrep` to build an index file:

```shell
sgrep -I -c OED2.idx -g sgml -i OED2.sgml
```

This might take a while. Once it’s done, put `OED2.idx` and `OED2.sgml` in a safe, static place where Emacs can see them.

### Building the headword index file

First you will probably want to byte- or native-compile `oed2.el`, or else it will take all day to generate this file. Then run:

```shell
emacs --batch -Q -L . --eval '(package-initialize)' -l oed2-generate-headword-index.el --eval '(setq oed2-sgml-location "~/OED2.sgml")' --eval '(setq oed2-index-location "~/OED2.idx")' -f oed2-write-headword-index
```

adjusting the file locations as necessary.

Optionally, you can gzip the resulting `oed2-headword-index.el`.

### Installing `oed2.el`

Put `oed2.el`, `oed2-specials.el`, and `oed2-headword-index.el` (and the byte compiled versions if any) somewhere where Emacs can see them and put this in your `init.el`:

```elisp
(require 'oed2)
;; set this to the location of your OED2.sgml file
(setq oed2-sgml-location "~/OED2.sgml")
;; set this to the location of your OED2.idx file
(setq oed2-index-location "~/OED2.idx")
;; set this to the location of your oed2-headword-index file
(setq oed2-headword-index-location "~/oed2-headword-index.el.gz")
```

## Usage

`M-x oed2` will prompt to search for an headword (`h`) or for quotations by author (`a`). This is a proof of concept. The sgrep index is rich enough that many other searches could be supported, but this is just a demonstration that recovering functionality from ‘dead’ dictionary databases is possible.

## TODO

- Handle (i.e. ignore) the `&sm;` and `&smm;` entities in headwords, somehow
  - Best option may actually be to create our own index without these (or any) entities and use `ivy-read` for the headword search
- Integration with `custom`
- Handle and appropriately style every tag in the OED
- Possibly kill the folding functionality and use `visual-line-mode` in result buffers instead
- Document things better
- Easy keyboard navigation
- Clickable cross-references
- More advanced search features

## Papers

Daphne Preston-Kendal (2022), <cite>Dictionaries in the Web of Alexandria</cite> (paper to be presented at the 12th International Conference on Historical Lexicography and Lexicology, Lorient, France 22 June 2022).
